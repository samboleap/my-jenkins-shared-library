def domainName = 'example.com'
def nginxConfPath = '/etc/nginx/nginx.conf'
def certbotCommand = "sudo certbot --nginx -d ${domainName} --non-interactive --agree-tos --email your-email@example.com"

// Generate Nginx configuration
def nginxConfig = """
server {
    listen 80;
    server_name ${domainName};

    location / {
        proxy_pass http://localhost:8081;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
    }
}

server {
    listen 443 ssl;
    server_name ${domainName};

    ssl_certificate /etc/letsencrypt/live/${domainName}/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/${domainName}/privkey.pem;

    location / {
        proxy_pass http://localhost:8080;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
    }
}
"""

// Write Nginx configuration to file
new File(nginxConfPath).withWriter { writer ->
    writer.write(nginxConfig)
}

// Reload Nginx configuration
def reloadCommand = 'sudo nginx -s reload'
def process = reloadCommand.execute()
process.waitFor()

// Run Certbot for SSL/TLS certificate
def certbotProcess = certbotCommand.execute()
certbotProcess.waitFor()

if (certbotProcess.exitValue() == 0) {
    println "Nginx configuration for ${domainName} has been successfully updated with Certbot integration."
} else {
    println "There was an error updating the Nginx configuration for ${domainName} with Certbot integration."
}