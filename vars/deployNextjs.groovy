def call(String minPort, String maxPort, String registryDocker, String buildContainerName, String containerName, String dockerTag, String mailSendTo, String telegramBotToken, String telegramChatId, String domainName, String email, String extraParam = null) {   
    try {
        def selectedPort = selectRandomAvailablePort(minPort.toInteger(), maxPort.toInteger())
        if (selectedPort) {
            echo "Selected port: $selectedPort"
            echo "Container Name: $containerName"
            pullDockerImage(registryDocker, buildContainerName, dockerTag)
            sh "docker run -d -p $selectedPort:3000 --name ${containerName}  ${registryDocker}/${buildContainerName}:${dockerTag}"
            sendMessageToTelegram("Docker Deploy Nextjs $selectedPort:3000 Successfully!", telegramBotToken, telegramChatId)
            sendMessageToGmail("Docker Deploy Nextjs $selectedPort:3000 Successfully!", mailSendTo)
            sendIpWithPort(selectedPort, telegramBotToken, telegramChatId)
        } else {
            error "No available ports found in the range $minPort-$maxPort"
        }
    } catch (Exception e) {
        sendMessageToTelegram("Error occurred during Docker Nextjs deployment: ${e.message}", telegramBotToken, telegramChatId)
        throw e
    }

    try {
        def usedPorts = listPortsInUseForDocker(minPort.toInteger(), maxPort.toInteger())
        if (!usedPorts.isEmpty()) {
            echo "Ports already in use for Docker port mapping on port 3000: ${usedPorts.join(', ')}"
            sendMessageToTelegram("Ports already in use for Docker port mapping on port 3000: ${usedPorts.join(', ')}", telegramBotToken, telegramChatId)
        }
    } catch (Exception e) {
        sendMessageToTelegram("Error occurred while listing used ports: ${e.message}", telegramBotToken, telegramChatId)
        throw e
    }
}

def pullDockerImage(registryDocker, imageName, tag) {
    try {
        sh "docker pull ${registryDocker}/${imageName}:${tag}"
    } catch (Exception e) {
        error "Failed to pull Docker image: ${e.message}"
    }
}

def createNginxConfig(domainName, selectedPort) {
    sh """
    cat > /etc/nginx/conf.d/${domainName}.conf <<EOF
    server {
        listen 80;
        server_name ${domainName};

        location / {
            proxy_pass http://localhost:${selectedPort};
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host \$host;
            proxy_cache_bypass \$http_upgrade;
        }
    }
    EOF
    """
}
def configureNginx(domainName, selectedPort) {
    try {
        createNginxConfig(domainName, selectedPort)
        sh "docker pull nginx:latest"
        sh "docker run -d -p 80:80 --name nginx -v /etc/nginx/conf.d:/etc/nginx/conf.d nginx:latest"
    } catch (Exception e) {
        println("Error occurred while configuring Nginx: ${e.message}")
    }
}

def configureCertbot(domainName, email) {
    try {
        sh "docker pull certbot/certbot:latest"
        sh "docker run --rm --name certbot -v /etc/letsencrypt:/etc/letsencrypt -v /var/lib/letsencrypt:/var/lib/letsencrypt certbot/certbot:latest certonly --standalone --email ${email} --agree-tos --no-eff-email -d ${domainName}"
    } catch (Exception e) {
        println("Error occurred while configuring Certbot: ${e.message}")
    }
}

def sendMessageToTelegram(message, telegramBotToken, telegramChatId) {
    try {
        sh "curl -s -X POST https://api.telegram.org/bot${telegramBotToken}/sendMessage -d chat_id=${telegramChatId} -d text='${message}'"
    } catch (Exception e) {
        println("Error occurred while sending Telegram message: ${e.message}")
    }
}

def sendMessageToGmail(message, mailSendTo) {
    try {
        mail bcc: '', body: message, cc: '', from: '', replyTo: '', subject: 'Hello', to: mailSendTo
    } catch (Exception e) {
        println("Error occurred while sending Gmail message: ${e.message}")
    }
}

def selectRandomAvailablePort(minPort, maxPort) {
    def numberOfPortsToCheck = maxPort - minPort + 1
    def portsToCheck = (minPort..maxPort).toList()
    Collections.shuffle(portsToCheck)

    for (int i = 0; i < numberOfPortsToCheck; i++) {
        def portToCheck = portsToCheck[i]
        if (isPortAvailable(portToCheck) && !isPortInUseForDocker(portToCheck)) {
            return portToCheck
        }
    }
    return null
}

def isPortAvailable(port) {
    def socket
    try {
        socket = new Socket("localhost", port)
        return false //if the Port is already in use
    } catch (Exception e) {
        return true //if the Port is available
    } finally {
        if (socket) {
            socket.close()
        }
    }
}

def isPortInUseForDocker(port) {
    def dockerPsOutput = sh(script: "docker ps --format '{{.Ports}}'", returnStdout: true).trim()
    return dockerPsOutput.contains(":$port->3000/tcp")
}

def listPortsInUseForDocker(minPort, maxPort) {
    def usedPorts = []
    for (int port = minPort; port <= maxPort; port++) {
        if (isPortInUseForDocker(port)) {
            usedPorts.add(port)
        }
    }
    return usedPorts
}

def sendIpWithPort(port, telegramBotToken, telegramChatId) {
    try {
        def ipAddress = sh(script: 'curl -s ifconfig.me', returnStdout: true).trim()
        def ipWithPort = "${ipAddress}:${port}"
        sendMessageToTelegram(ipWithPort, telegramBotToken, telegramChatId)
    } catch (Exception e) {
        println("Error occurred while sending IP with port information: ${e.message}")
    }
}