@Library("my-jenkins-shared-library") _

pipeline {
    agent {
        label 'samboleap'
    }

    parameters {
        booleanParam(
            name: 'dockerBuild',
            defaultValue: true,
            description: 'Build Docker Image'
        )
        booleanParam(
            name: 'deployDocker',
            defaultValue: true,
            description: 'Docker Deploy'
        )
        choice(
            name: "choice",
            choices: ["main", "master"],
            description: "Sample multi-choice parameter"
        )
        string(
            name: 'registryDocker',
            defaultValue: 'samboleap',
            description: 'Registry'
        )
        string(
            name: 'buildContainerName',
            defaultValue: 'gradlebuild',
            description: 'Container'
        )
        string(
            name: 'containerName',
            defaultValue: generateContainerNameBackEnd(),
            description: 'Backend Container'
        )
        string(
            name: 'dockerTag',
            defaultValue: 'latest',
            description: 'Docker Tag'
        )
        string(
            name: 'repoUrl',
            defaultValue: 'https://gitlab.com/samboleap/spring-swagger',
            description: 'Repository URL'
        )
        string(
            name: 'domainName',
            defaultValue: "gradle.samboleap.com",
            description: "Configure domain name"
        )
        string(
            name: 'email',
            defaultValue: "samboleap3008@gmail.com",
            description: "certbot email"
        )
    }

    environment {
        telegramBotToken = '6678469501:AAGO8syPMTxn0gQGksBPRchC-EoC6QRoS5o'
        telegramChatId = '-4016725356'
        username = "${params.username}"
        registryDocker = "${params.registryDocker}"
        buildContainerName = "${params.buildContainerName}"
        containerName = "${params.containerName}"
        dockerTag = "${params.dockerTag}"
        mailSendTo = 'samboleap3008@gmail.com'
        minPort = '8081'
        maxPort = '8091'
        repoUrl = "${params.repoUrl}"
        gitCredential = 'password_for_gitlab'
        branch = 'main'
        email= 'samboleap3008@gmail.com'
        domainName = "${params.domainName}"
    }

    stages {
        stage(' Clean Workspace'){
            steps{
                cleanWs()
            }
        }
        stage('Get Code from SCM') {
            steps {
                echo "choice is ${choice}"
                script {
                    gitRepo(
                        repoUrl,
                        gitCredential,
                        branch,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }

        stage('Build') {
            steps {
                echo "Building Images for deploying"
                script {
                    def dockerfileContent = '''
                        # Build stage
                        FROM gradle:jdk17 AS builder
                        WORKDIR /
                        #Copy all files and directories from the current directory to /app
                        RUN gradle --no-daemon --version
                        COPY . .
                        RUN gradle build --no-daemon

                        # Final stage
                        FROM openjdk:17
                        COPY --from=builder /build/libs/*-SNAPSHOT.jar /app.jar
                        EXPOSE 8081
                        VOLUME /src/main/resources/images
                        ENTRYPOINT ["java", "-jar", "/app.jar"]
                    '''

                    // Write the Dockerfile content to a file
                    writeFile file: 'Dockerfile', text: dockerfileContent

                    // Build the Docker image using the specified Dockerfile
                    def dockerImage = docker.build("springboot", "-f Dockerfile .")

                    buildSpringGradle(
                        registryDocker,
                        buildContainerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }

        stage('Deploy Docker agent 1') {
            agent {
                label 'worker_agent1' 
            }
            steps {
                script {
                    deploySpringGradle(
                        minPort,
                        maxPort,
                        registryDocker,
                        buildContainerName,
                        containerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId,
                        mailSendTo,
                        domainName,
                        email
                    )
                }
            }
        }
        stage('Deploy Docker agent 2') {
            agent {
                label 'worker_agent2' 
            }
            steps {
                script {
                    deploySpringGradle(
                        minPort,
                        maxPort,
                        registryDocker,
                        buildContainerName,
                        containerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId,
                        mailSendTo,
                        domainName,
                        email
                    )
                }
            }
        }
    }
}

def generateContainerNameBackEnd() {
    // Generate a dynamic default value, for example, based on a timestamp or a random value
    return "gradle-${new Date().getTime()}"
}