@Library("my-jenkins-shared-library") _

pipeline {
    agent {
        label 'samboleap'
    }

    parameters {
        booleanParam(
            name: 'dockerBuild',
            defaultValue: true,
            description: 'Build Docker Image'
        )
        booleanParam(
            name: 'deployDocker',
            defaultValue: true,
            description: 'Docker Deploy'
        )
        choice(
            name: "choice",
            choices: ["main", "master"],
            description: "Sample multi-choice parameter"
        )
        string(
            name: 'registryDocker',
            defaultValue: 'samboleap',
            description: 'Registry'
        )
        string(
            name: 'buildContainerName',
            defaultValue: 'mavenbuild',
            description: 'Container'
        )
        string(
            name: 'containerName',
            defaultValue: generateContainerNameBackEnd(),
            description: 'Backend Container'
        )
        string(
            name: 'dockerTag',
            defaultValue: 'latest',
            description: 'Docker Tag'
        )
        string(
            name: 'repoUrl',
            defaultValue: 'https://github.com/begoingto/spring-boot-maven-devops',
            description: 'Repository URL'
        )
        string(
            name: 'domainName',
            defaultValue: "maven.samboleap.com",
            description: "Configure domain name"
        )
        string(
            name: 'email',
            defaultValue: "samboleap3008@gmail.com",
            description: "certbot email"
        )
    }

    environment {
        telegramBotToken = '6678469501:AAGO8syPMTxn0gQGksBPRchC-EoC6QRoS5o'
        telegramChatId = '-4016725356'
        username = "${params.username}"
        registryDocker = "${params.registryDocker}"
        buildContainerName = "${params.buildContainerName}"
        containerName = "${params.containerName}"
        dockerTag = "${params.dockerTag}"
        mailSendTo = 'samboleap3008@gmail.com'
        minPort = '8081'
        maxPort = '8091'
        repoUrl = "${params.repoUrl}"
        gitCredential = 'password_for_gitlab'
        branch = 'master'
        email= 'samboleap3008@gmail.com'
        domainName = "${params.domainName}"
    }

    stages {
        stage(' Clean Workspace'){
            steps{
                cleanWs()
            }
        }
        stage('Get Code from SCM') {
            steps {
                echo "choice is ${choice}"
                script {
                    gitRepo(
                        repoUrl,
                        gitCredential,
                        branch,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }

        stage('Build') {
            steps {
                echo "Building Images for deploying"
                script {
                    def dockerfileContent = '''
                        FROM adoptopenjdk:11-jre-hotspot as builder
                        COPY target/*.jar application.jar
                        RUN java -Djarmode=layertools -jar application.jar extract

                        FROM adoptopenjdk:11-jre-hotspot
                        COPY --from=builder dependencies/ ./
                        COPY --from=builder snapshot-dependencies/ ./
                        COPY --from=builder spring-boot-loader/ ./
                        COPY --from=builder application/ ./
                        ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
                    '''

                    // Write the Dockerfile content to a file
                    writeFile file: 'Dockerfile', text: dockerfileContent

                    // Build the Docker image using the specified Dockerfile
                    def dockerImage = docker.build(env.buildContainerName, "-f Dockerfile .")

                    buildSpringMaven(
                        registryDocker,
                        buildContainerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId
                    )
                }
            }
        }

        stage('Deploy Docker agent 1') {
            agent {
                label 'worker_agent1' 
            }
            steps {
                script {
                    deploySpringMaven(
                        minPort,
                        maxPort,
                        registryDocker,
                        buildContainerName,
                        containerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId,
                        domainName,
                        email
                    )
                }
            }
        }
        stage('Deploy Docker agent 2') {
            agent {
                label 'worker_agent2' 
            }
            steps {
                script {
                    deploySpringMaven(
                        minPort,
                        maxPort,
                        registryDocker,
                        buildContainerName,
                        containerName,
                        dockerTag,
                        mailSendTo,
                        telegramBotToken,
                        telegramChatId,
                        domainName,
                        email
                    )
                }
            }
        }

    }
}

def generateContainerNameBackEnd() {
    // Generate a dynamic default value, for example, based on a timestamp or a random value
    return "maven-${new Date().getTime()}"
}